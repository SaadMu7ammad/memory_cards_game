## Memory Cards

Flash card app for learning. Display, add and remove memory cards with questions and answers

## Project Specifications

- Create flip cards using CSS
- Create "Add new card" overlay with form
- Display question cards and flip for answer
- View prev and next cards
- Add new cards to local storage
- Clear all cards from local storage


[check it out](https://memory-cards-game-saadmu7ammad-3c70704b94c68bde10468dcfd46474b8.gitlab.io/)